export interface AppConfig {
  documentationURL: string;
  skaoHomepage: string;
  appTitle: { name: string, class: string }[];
}
