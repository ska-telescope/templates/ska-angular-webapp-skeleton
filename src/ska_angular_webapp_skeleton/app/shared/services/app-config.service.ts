import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppConfig } from '../models/configuration.models';

@Injectable({
  providedIn: 'root',
})
export class AppConfigService {
  private configuration!: AppConfig;

  constructor(private http: HttpClient) {}

  //developers can choose to improve this to load configuration from an external service and then fallback to local configuration file
  loadConfiguration() {
    return new Promise((resolve) => {
      this.http
        .get<AppConfig>('/assets/configuration/configuration.json')
        .subscribe((response) => {
          this.configuration = response;
          resolve(true);
        });
    });
  }

  getConfiguration(): AppConfig | null {
    if (this.configuration) {
      return this.configuration;
    }
    return null;
  }
}
