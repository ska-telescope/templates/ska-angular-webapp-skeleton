import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as defaultLanguage from '../../../assets/i18n/en.json';
import { Languages } from '../models/language.models';

@Injectable({
  providedIn: 'root',
})
export class TranslationService {
  constructor(private translate: TranslateService) {
    translate.setTranslation('en', defaultLanguage);
    translate.setDefaultLang('en');
  }

  getAvailableLanguages(): Array<Languages> {
    //this would be added to config file
    return [
      { code: 'en', title: 'English' },
      { code: 'de', title: 'Deutsch' },
      { code: 'fr', title: 'Français' },
    ];
  }

  useLanguage(language: string): void {
    this.translate.use(language);
  }
}
